package ru.laptiev.books.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.laptiev.books.api.service.IStoreService;
import ru.laptiev.books.entity.Store;

import java.util.List;

@Slf4j
@Api("Database main rest controller")
@RestController
@RequestMapping("/store")
public class StoreController {

    private static final String GET_BY_ID = "/{id}";
    private static final String ALL = "/all";

    private IStoreService storeService;

    public IStoreService getStoreService() {
        return storeService;
    }

    @Autowired
    public void setStoreService(final IStoreService storeService) {
        this.storeService = storeService;
    }

    @ApiOperation(
            value = "Поиск магазина по id",
            notes = "Возвращает магазин")
    @GetMapping(GET_BY_ID)
    public Store findOne(@PathVariable(value = "id") @ApiParam(value = "Store UID", example = "0") @Nullable final Long id) {
        return storeService.findOne(id);
    }

    @ApiOperation(
            value = "Сохранение магазина в БД",
            notes = "Notes will be here")
    @PostMapping
    public void saveOne(@RequestBody @Nullable final Store store) {
        storeService.save(store);
    }

    @ApiOperation(
            value = "Удаление магазина по id",
            notes = "Notes will be here")
    @DeleteMapping(GET_BY_ID)
    public void deleteOne(@PathVariable(value = "id") @ApiParam(value = "Store UID", example = "0") @Nullable final Long id) {
        storeService.remove(id);
    }

    @ApiOperation(
            value = "Поиск всех магазинов",
            notes = "Возвращает лист магазинов")
    @GetMapping(ALL)
    public List<Store> findAll() {
        return storeService.findAll();
    }
}
