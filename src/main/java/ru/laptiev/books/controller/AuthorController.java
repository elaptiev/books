package ru.laptiev.books.controller;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.laptiev.books.api.service.IAuthorService;
import ru.laptiev.books.entity.Author;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Slf4j
@Api("Database main rest controller")
@RestController
@RequestMapping("/author")
public class AuthorController {

    private static final String GET_BY_ID = "/{id}";
    private static final String GET_BY_NAME = "/name/{name}";
    private static final String ALL = "/all";

    private IAuthorService authorService;

    public IAuthorService getAuthorService() {
        return authorService;
    }

    @Autowired
    public void setAuthorService(final IAuthorService authorService) {
        this.authorService = authorService;
    }

    @ApiOperation(
            value = "Поиск автора по id",
            notes = "Возвращает автора")
    @GetMapping(GET_BY_ID)
    public Author findOne(@PathVariable(value = "id") @ApiParam(value = "Author UID", example = "0") @Nullable final Long id) {
        return authorService.findOne(id);
    }

    @ApiOperation(
            value = "Сохранение автора в БД",
            notes = "Notes will be here")
    @PostMapping
    public void saveOne(@RequestBody @Nullable final Author author) {
        authorService.save(author);
    }

    @ApiOperation(
            value = "Удаление автора по id",
            notes = "Notes will be here")
    @DeleteMapping(GET_BY_ID)
    public void deleteOne(@PathVariable(value = "id") @ApiParam(value = "Author UID", example = "0") @Nullable final Long id) {
        authorService.remove(id);
    }

    @ApiOperation(
            value = "Поиск автора по имени",
            notes = "Возвращает автора")
    @GetMapping(GET_BY_NAME)
    public Author findOneByName(@PathVariable(value = "name") @ApiParam(value = "Author name") @Nullable final String name) {
        return authorService.findOneByName(name);
    }

    @ApiOperation(
            value = "Поиск всех авторов",
            notes = "Возвращает лист авторов")
    @GetMapping(ALL)
    public List<Author> findAll() {
        return authorService.findAll();
    }
}
