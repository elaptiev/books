package ru.laptiev.books.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.laptiev.books.api.service.IBookService;
import ru.laptiev.books.entity.Book;

import java.util.List;

@Slf4j
@Api("Database main rest controller")
@RestController
@RequestMapping("/book")
public class BookController {

    private static final String GET_BY_ID = "/{id}";
    private static final String ALL = "/all";

    private IBookService bookService;

    public IBookService getBookService() {
        return bookService;
    }

    @Autowired
    public void setBookService(final IBookService bookService) {
        this.bookService = bookService;
    }

    @ApiOperation(
            value = "Поиск книгу по id",
            notes = "Возвращает книгу")
    @GetMapping(GET_BY_ID)
    public Book findOne(@PathVariable(value = "id") @ApiParam(value = "Book UID", example = "0") @Nullable final Long id) {
        return bookService.findOne(id);
    }

    @ApiOperation(
            value = "Сохранение книги в БД",
            notes = "Notes will be here")
    @PostMapping
    public void saveOne(@RequestBody @Nullable final Book book) {
        bookService.save(book);
    }

    @ApiOperation(
            value = "Удаление книги по id",
            notes = "Notes will be here")
    @DeleteMapping(GET_BY_ID)
    public void deleteOne(@PathVariable(value = "id") @ApiParam(value = "Book UID", example = "0") @Nullable final Long id) {
        bookService.remove(id);
    }

    @ApiOperation(
            value = "Поиск всех книг",
            notes = "Возвращает лист книг")
    @GetMapping(ALL)
    public List<Book> findAll() {
        return bookService.findAll();
    }
}
