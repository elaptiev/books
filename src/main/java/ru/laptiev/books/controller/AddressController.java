package ru.laptiev.books.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.laptiev.books.api.service.IAddressService;
import ru.laptiev.books.entity.Address;

import java.util.List;

@Slf4j
@Api("Database main rest controller")
@RestController
@RequestMapping("/address")
public class AddressController {
    
    private static final String GET_BY_ID = "/{id}";
    private static final String ALL = "/all";

    private IAddressService addressService;

    public IAddressService getAddressService() {
        return addressService;
    }

    @Autowired
    public void setAddressService(final IAddressService addressService) {
        this.addressService = addressService;
    }

    @ApiOperation(
            value = "Поиск адреса по id",
            notes = "Возвращает адрес")
    @GetMapping(GET_BY_ID)
    public Address findOne(@PathVariable(value = "id") @ApiParam(value = "Address UID", example = "0") @Nullable final Long id) {
        return addressService.findOne(id);
    }

    @ApiOperation(
            value = "Сохранение адреса в БД",
            notes = "Notes will be here")
    @PostMapping
    public void saveOne(@RequestBody @Nullable final Address address) {
        addressService.save(address);
    }

    @ApiOperation(
            value = "Удаление адреса по id",
            notes = "Notes will be here")
    @DeleteMapping(GET_BY_ID)
    public void deleteOne(@PathVariable(value = "id") @ApiParam(value = "Address UID", example = "0") @Nullable final Long id) {
        addressService.remove(id);
    }

    @ApiOperation(
            value = "Поиск всех адресов",
            notes = "Возвращает лист адресов")
    @GetMapping(ALL)
    public List<Address> findAll() {
        return addressService.findAll();
    }
}
