package ru.laptiev.books.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Proxy(lazy = false)
@Table(name = "app_address")
public class Address extends AbstractEntity {

    @Nullable
    @Column(name = "city")
    private String city;

    @Nullable
    @Column(name = "street")
    private String street;

    @Nullable
    @Column(name = "house_number")
    private Integer houseNumber;

    @Nullable
    @Transient
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Store> storeList;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }
}
