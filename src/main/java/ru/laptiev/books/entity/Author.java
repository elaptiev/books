package ru.laptiev.books.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Proxy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Proxy(lazy = false)
@Table(name = "app_author")
public class Author extends AbstractEntity {

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @JsonManagedReference
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<Book> bookList;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

    @Nullable
    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(@Nullable final List<Book> bookList) {
        this.bookList = bookList;
    }
}
