package ru.laptiev.books.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Proxy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Proxy(lazy = false)
@Table(name = "app_book")
public class Book extends AbstractEntity {

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "description")
    private String description;

    @Nullable
    @JsonBackReference
    @JoinColumn(name = "author_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Author author;

    @Nullable
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "app_book_store",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "store_id")
    )
    private List<Store> storeList;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable final String description) {
        this.description = description;
    }

    @Nullable
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(@Nullable final Author author) {
        this.author = author;
    }

    @Nullable
    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(@Nullable final List<Store> storeList) {
        this.storeList = storeList;
    }
}
