package ru.laptiev.books.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Proxy(lazy = false)
@Table(name = "app_store")
public class Store extends AbstractEntity {

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "app_store_address",
            joinColumns = @JoinColumn(name = "store_id"),
            inverseJoinColumns = @JoinColumn(name = "address_id")
    )
    private List<Address> addressList;

    @Nullable
    @Transient
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Book> bookList;

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable final String name) {
        this.name = name;
    }

    @Nullable
    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(@Nullable final List<Address> addressList) {
        this.addressList = addressList;
    }

    @Nullable
    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(@Nullable final List<Book> bookList) {
        this.bookList = bookList;
    }
}
