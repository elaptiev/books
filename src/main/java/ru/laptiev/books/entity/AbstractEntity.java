package ru.laptiev.books.entity;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    public Long getId() {
        return id;
    }

    public void setId(@NotNull final Long id) {
        this.id = id;
    }
}
