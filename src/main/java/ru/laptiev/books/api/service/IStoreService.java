package ru.laptiev.books.api.service;

import org.jetbrains.annotations.Nullable;
import ru.laptiev.books.entity.Store;

import java.util.List;

public interface IStoreService {
    @Nullable Store findOne(@Nullable Long id);

    @Nullable List<Store> findAll();

    @Nullable Store save(@Nullable Store store);

    void remove(@Nullable Long id);

    void removeAll();
}
