package ru.laptiev.books.api.service;

import org.jetbrains.annotations.Nullable;
import ru.laptiev.books.entity.Author;

import java.util.List;

public interface IAuthorService {
    @Nullable Author findOne(@Nullable Long id);

    @Nullable Author findOneByName(@Nullable String name);

    @Nullable List<Author> findAll();

    @Nullable Author save(@Nullable Author author);

    void remove(@Nullable Long id);

    void removeAll();
}
