package ru.laptiev.books.api.service;

import org.jetbrains.annotations.Nullable;
import ru.laptiev.books.entity.Book;

import java.util.List;

public interface IBookService {
    @Nullable Book findOne(@Nullable Long id);

    @Nullable List<Book> findAll();

    @Nullable Book save(@Nullable Book book);

    void remove(@Nullable Long id);

    void removeAll();
}
