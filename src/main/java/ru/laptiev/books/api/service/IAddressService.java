package ru.laptiev.books.api.service;

import org.jetbrains.annotations.Nullable;
import ru.laptiev.books.entity.Address;

import java.util.List;

public interface IAddressService {
    @Nullable Address findOne(@Nullable Long id);

    @Nullable List<Address> findAll();

    @Nullable Address save(@Nullable Address address);

    void remove(@Nullable Long id);

    void removeAll();
}
