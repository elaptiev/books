package ru.laptiev.books.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.laptiev.books.entity.Author;

@Repository
public interface IAuthorRepository extends JpaRepository<Author, Long> {

    @Nullable
    @Query(value = "SELECT a FROM Author a WHERE a.name=:name")
    Author findOneByName(@Param("name") @NotNull String name);
}
