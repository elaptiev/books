package ru.laptiev.books.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.laptiev.books.entity.Store;

@Repository
public interface IStoreRepository extends JpaRepository<Store, Long> {
}
