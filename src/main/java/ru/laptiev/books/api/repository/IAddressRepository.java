package ru.laptiev.books.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.laptiev.books.entity.Address;

@Repository
public interface IAddressRepository extends JpaRepository<Address, Long> {
}
