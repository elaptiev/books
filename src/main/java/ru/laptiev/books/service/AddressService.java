package ru.laptiev.books.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.laptiev.books.api.repository.IAddressRepository;
import ru.laptiev.books.api.service.IAddressService;
import ru.laptiev.books.entity.Address;

import java.util.List;

@Service
public class AddressService implements IAddressService {
    
    private IAddressRepository addressRepository;

    public IAddressRepository getAddressRepository() {
        return addressRepository;
    }

    @Autowired
    public void setAddressRepository(final IAddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public @Nullable Address findOne(@Nullable Long id) {
        if (id == null) return null;
        if (addressRepository.existsById(id)) return addressRepository.getOne(id);
        return null;
    }

    @Override
    public @Nullable List<Address> findAll() {
        @NotNull final List<Address> list = addressRepository.findAll();
        return list.isEmpty() ? null : list;
    }

    @Override
    public @Nullable Address save(@Nullable Address address) {
        if (address == null) return null;
        return addressRepository.save(address);
    }

    @Override
    public void remove(@Nullable Long id) {
        if (id == null) return;
        addressRepository.deleteById(id);
    }

    @Override
    public void removeAll() {
        addressRepository.deleteAll();
    }
}
