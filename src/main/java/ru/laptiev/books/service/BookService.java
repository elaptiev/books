package ru.laptiev.books.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.laptiev.books.api.repository.IBookRepository;
import ru.laptiev.books.api.service.IBookService;
import ru.laptiev.books.entity.Book;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BookService implements IBookService {

    private IBookRepository bookRepository;

    public IBookRepository getBookRepository() {
        return bookRepository;
    }

    @Autowired
    public void setBookRepository(final IBookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    @Nullable
    public Book findOne(@Nullable final Long id) {
        if (id == null) return null;
        if (bookRepository.existsById(id)) return bookRepository.getOne(id);
        return null;
    }

    @Override
    @Nullable
    public List<Book> findAll() {
        @NotNull final List<Book> list = bookRepository.findAll();
        return list.isEmpty() ? null : list;
    }

    @Override
    @Nullable
    public Book save(@Nullable final Book book) {
        if (book == null) return null;
        return bookRepository.save(book);
    }

    @Override
    public void remove(@Nullable final Long id) {
        if (id == null) return;
        bookRepository.deleteById(id);
    }

    @Override
    public void removeAll() {
        bookRepository.deleteAll();
    }
}
