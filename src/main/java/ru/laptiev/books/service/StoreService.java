package ru.laptiev.books.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.laptiev.books.api.repository.IStoreRepository;
import ru.laptiev.books.api.service.IStoreService;
import ru.laptiev.books.entity.Store;

import java.util.List;

@Service
public class StoreService implements IStoreService {

    private IStoreRepository storeRepository;

    public IStoreRepository getStoreRepository() {
        return storeRepository;
    }

    @Autowired
    public void setStoreRepository(final IStoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    @Override
    public @Nullable Store findOne(@Nullable Long id) {
        if (id == null) return null;
        if (storeRepository.existsById(id)) return storeRepository.getOne(id);
        return null;
    }

    @Override
    public @Nullable List<Store> findAll() {
        @NotNull final List<Store> list = storeRepository.findAll();
        return list.isEmpty() ? null : list;
    }

    @Override
    public @Nullable Store save(@Nullable Store store) {
        if (store == null) return null;
        return storeRepository.save(store);
    }

    @Override
    public void remove(@Nullable Long id) {
        if (id == null) return;
        storeRepository.deleteById(id);
    }

    @Override
    public void removeAll() {
        storeRepository.deleteAll();
    }
}
