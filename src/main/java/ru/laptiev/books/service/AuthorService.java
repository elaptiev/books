package ru.laptiev.books.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.laptiev.books.api.repository.IAuthorRepository;
import ru.laptiev.books.api.service.IAuthorService;
import ru.laptiev.books.entity.Author;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AuthorService implements IAuthorService {

    private IAuthorRepository authorRepository;

    public IAuthorRepository getAuthorRepository() {
        return authorRepository;
    }

    @Autowired
    public void setAuthorRepository(final IAuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    @Nullable
    public Author findOne(@Nullable final Long id) {
        if (id == null) return null;
        if (authorRepository.existsById(id)) return authorRepository.getOne(id);
        return null;
    }

    @Override
    @Nullable
    public Author findOneByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return authorRepository.findOneByName(name);
    }

    @Override
    @Nullable
    public List<Author> findAll() {
        @NotNull final List<Author> list = authorRepository.findAll();
        return list.isEmpty() ? null : list;
    }

    @Override
    @Nullable
    public Author save(@Nullable final Author author) {
        if (author == null) return null;
        return authorRepository.save(author);
    }

    @Override
    public void remove(@Nullable final Long id) {
        if (id == null) return;
        authorRepository.deleteById(id);
    }

    @Override
    public void removeAll() {
        authorRepository.deleteAll();
    }
}
