### User creation
```
CREATE USER books
    WITH ENCRYPTED PASSWORD 'jXZSk89waaVTBsTz';
```
### Database creation
```
CREATE DATABASE books
    WITH 
    OWNER = books
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
```