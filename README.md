### Books API

### Software requirements

* [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html) - Java SE Development Kit
* [PostgreSQL 11](https://www.postgresql.org/download/) - Open Source Relational Database Server 
* [Gradle 6.2.1](https://docs.gradle.org/current/userguide/installation.html) - Open Source Build Automation Tool


### Swagger url
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)